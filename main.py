from tkinter import *
from tkinter import messagebox
import base64


def encode(key, clear):
    enc = []
    for i in range(len(clear)):
        key_c = key[i % len(key)]
        enc_c = chr((ord(clear[i]) + ord(key_c)) % 256)
        enc.append(enc_c)
    return base64.urlsafe_b64encode("".join(enc).encode()).decode()
def decode(key, enc):
    dec = []
    enc = base64.urlsafe_b64decode(enc).decode()
    for i in range(len(enc)):
        key_c = key[i % len(key)]
        dec_c = chr((256 + ord(enc[i]) - ord(key_c)) % 256)
        dec.append(dec_c)
    return "".join(dec)
def save():
    title = title_entry.get()
    message = note_text.get("1.0", END)
    key = key_entry.get()

    if len(title) == 0 or len(message) == 0 or len(key) == 0:
        messagebox.showwarning(title='Error!', message='Please enter all information.')
    else:
        message_encrypted = encode(key, message)

        try:
            with open('notes.txt', 'a') as file:
                file.write(f"\n{title}\n{message_encrypted}\n------------------")
        except FileNotFoundError:
            with open("notes.txt", "w") as data_file:
                data_file.write(f"{title}\n{message_encrypted}\n------------------")
        finally:
            title_entry.delete(0, END)
            key_entry.delete(0, END)
            note_text.delete("1.0", END)
            messagebox.showinfo(title='Done', message='Done!')

def translate():
    message = note_text.get("1.0", END)
    key = key_entry.get()
    if len(message) == 0 or len(key) == 0:
        messagebox.showwarning(title='Error!', message='Please enter all information.')
    else:
        message_decrypted = decode(key, message)
        key_entry.delete(0, END)
        title_entry.delete(0, END)
        note_text.delete("1.0", END)
        note_text.insert("1.0", message_decrypted)

# ui
window = Tk()
window.config(width='400', height='800', pady=20, padx=50)
window.resizable(False,False)
# window.geometry('1920x1080')
window.title('Secret Notes')

canvas = Canvas(height=200, width=200)
logo = PhotoImage(file='topsecret.png')
canvas.create_image(100, 100, image=logo)
canvas.pack()

title_label = Label(text="Enter your title")
title_label.pack()
title_entry = Entry(width=40, font=('Verdana', 10, 'bold'), justify='center')
title_entry.pack()

note_label = Label(text="Enter your note")
note_label.pack()
note_text = Text(padx=10, pady=10, width=50, font=('Verdana', 10, 'normal'))
note_text.pack()

key_label = Label(text="Enter your master key")
key_label.pack()
key_entry = Entry(width=50)
key_entry.pack()

canvas = Canvas(height=10, width=0)
canvas.pack()

save_button = Button(text="Save & Encrypt", background='light green', command=save)
save_button.pack()

canvas = Canvas(height=10, width=0)
canvas.pack()

decrypt_button = Button(text="Decrypt", background='light pink', command=translate)
decrypt_button.pack()

window.mainloop()
